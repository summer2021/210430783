package commands

import (
	"github.com/210430783/cli"
)

var Commands = []*cli.Command{
	{
		Name:  "web",
		Short: "\tStart the web server",
		Options: []*cli.Option{
			{
				Names: []string{"a", "addr"},
				Usage: "\tListen to the specified address",
			},
			{
				Names: []string{"d", "daemon"},
				Usage: "\tRun in the background",
			},
		},
		RunI: &WebCommand{},
	},
}
