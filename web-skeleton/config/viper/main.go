package viper

import (
	"fmt"
	"github.com/210430783/cli/argv"
	"github.com/210430783/web-skeleton/config"
	"github.com/spf13/viper"
)

func init() {
	// Conf support JSON, TOML, YAML, HCL, INI, envfile
	viper.AddConfigPath(fmt.Sprintf("%s/../conf/config.yml", argv.Program().Dir))
	if err := viper.ReadInConfig(); err != nil {
		panic(err)
	}
	if err := viper.Unmarshal(&config.Config); err != nil {
		panic(err)
	}
}
