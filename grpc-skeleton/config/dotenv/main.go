package dotenv

import (
	"fmt"
	"github.com/210430783/cli/argv"
	"github.com/210430783/dotenv"
)

func init() {
	// Env
	if err := dotenv.Load(fmt.Sprintf("%s/../.env", argv.Program().Dir)); err != nil {
		panic(err)
	}
}
