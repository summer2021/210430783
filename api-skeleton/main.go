package main

import (
	"github.com/210430783/api-skeleton/commands"
	_ "github.com/210430783/api-skeleton/config/configor"
	_ "github.com/210430783/api-skeleton/config/dotenv"
	_ "github.com/210430783/api-skeleton/di"
	"github.com/210430783/cli"
	"github.com/210430783/dotenv"
)

func main() {
	cli.SetName("app").
		SetVersion("0.0.0-alpha").
		SetDebug(dotenv.Getenv("APP_DEBUG").Bool(false))
	cli.AddCommand(commands.Commands...).Run()
}
