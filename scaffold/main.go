package main

import (
	"github.com/210430783/cli"
	"github.com/210430783/dotenv"
	"github.com/210430783/scaffold/commands"
)

func main() {
	cli.SetName("Dubbo-gocli").
		SetVersion(commands.CLIVersion).
		SetDebug(dotenv.Getenv("APP_DEBUG").Bool(false))
	cli.AddCommand(commands.Cmds...).Run()
}
