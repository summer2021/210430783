package commands

import "github.com/210430783/cli"

var Cmds = []*cli.Command{
	{
		Name:  "new",
		Short: "\tCreate a project",
		RunI:  &NewCommand{},
	},
}
